package mt.api;

import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.client.annotation.Client;
import mt.api.model.Account;
import mt.api.model.TransferRequest;

import java.math.BigDecimal;

@Client("/api")
public interface TestApiClient {

    @Get("/accounts/{id}")
    Account getAccount(final String id);

    default BigDecimal getBalance(final String accountId) {
        final Account account = getAccount(accountId);
        return account.getBalance();
    }

    @Post("/transfers")
    void transfer(final TransferRequest request);

}
