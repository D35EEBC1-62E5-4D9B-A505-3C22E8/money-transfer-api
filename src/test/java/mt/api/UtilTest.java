package mt.api;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigDecimal;

import static java.math.BigDecimal.*;
import static mt.api.Util.*;
import static org.assertj.core.api.Assertions.*;

public final class UtilTest {

    public static final BigDecimal MINUS_TEN = TEN.negate();


    @Test
    void isGreaterThanReturnsTrueIfFirstArgumentIsGreaterThanSecond() {
        assertThat(isGreaterThan(TEN, ONE)).isTrue();
    }

    @Test
    void isGreaterThanReturnsFalseIfFirstArgumentIsEqualToSecond() {
        assertThat(isGreaterThan(TEN, TEN)).isFalse();
    }

    @Test
    void isGreaterThanReturnsFalseIfFirstArgumentIsLessThanSecond() {
        assertThat(isGreaterThan(ONE, TEN)).isFalse();
    }

    @Test
    void isGreaterThrowsNullPointerExceptionIfFirstArgumentIsNullAndSecondIsNot() {
        assertThatNullPointerException().isThrownBy(() -> isGreaterThan(null, TEN));
    }

    @Test
    void isGreaterThrowsNullPointerExceptionIfFirstArgumentIsNotNullAndSecondIs() {
        assertThatNullPointerException().isThrownBy(() -> isGreaterThan(ONE, null));
    }

    @Test
    void isGreaterThrowsNullPointerExceptionIfBothArgumentsAreNull() {
        assertThatNullPointerException().isThrownBy(() -> isGreaterThan(null, null));
    }


    @Test
    void isPositiveReturnsTrueIfArgumentIsGreaterThanZero() {
        assertThat(isPositive(TEN)).isTrue();
    }

    @Test
    void isPositiveReturnsFalseIfArgumentIsEqualToZero() {
        assertThat(isPositive(ZERO)).isFalse();
    }

    @Test
    void isPositiveReturnsFalseIfArgumentIsLessThanZero() {
        assertThat(isPositive(MINUS_TEN)).isFalse();
    }

    @Test
    void isPositiveThrowsNullPointerExceptionIfArgumentIsNull() {
        assertThatNullPointerException().isThrownBy(() -> isPositive(null));
    }


    @Test
    void requirePositiveThrowsIllegalArgumentExceptionIfArgumentIsNegative() {
        assertThatIllegalArgumentException().isThrownBy(() -> requirePositive(MINUS_TEN));
    }

    @Test
    void requirePositiveThrowsIllegalArgumentExceptionIfArgumentIsEqualToZero() {
        assertThatIllegalArgumentException().isThrownBy(() -> requirePositive(ZERO));
    }

    @Test
    void requirePositiveThrowsIllegalArgumentExceptionIfArgumentIsNull() {
        assertThatIllegalArgumentException().isThrownBy(() -> requirePositive(null));
    }

    @Test
    void requirePositiveReturnsIfArgumentIsPositive() {
        assertThatCode(() -> requirePositive(TEN)).doesNotThrowAnyException();
    }

    @Test
    void areEqualReturnsTrueIfArgumentAreEqualButHaveDifferentScale() {
        final BigDecimal left = new BigDecimal("12.34");
        final BigDecimal right = new BigDecimal("12.340");

        assertThat(areEqual(left, right)).isTrue();
    }

    @ParameterizedTest
    @ValueSource(strings = {"123", "456.0", "0.000000001"})
    void isNonNegativeReturnsTrueIfArgumentIsPositive(final BigDecimal number) {
        assertThat(isNonNegative(number)).isTrue();
    }

    @ParameterizedTest
    @ValueSource(strings = {"0", "0.000000", "000000.0"})
    void isNonNegativeReturnsTrueIfArgumentIsZero(final BigDecimal number) {
        assertThat(isNonNegative(number)).isTrue();
    }

    @ParameterizedTest
    @ValueSource(strings = {"-123", "-456.0", "-0.000000001"})
    void isNonNegativeReturnsFalseIfArgumentIsPositive(final BigDecimal number) {
        assertThat(isNonNegative(number)).isFalse();
    }

}
