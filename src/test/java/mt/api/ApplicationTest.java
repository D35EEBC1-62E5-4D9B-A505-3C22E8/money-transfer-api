package mt.api;

import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.test.annotation.MicronautTest;
import mt.api.model.TransferRequest;
import org.assertj.core.api.Condition;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@MicronautTest
public final class ApplicationTest {

    @Inject
    TestApiClient testApiClient;

    @Test
    @DisplayName("Successful request")
    void test0() {
        final String senderId = "sender";
        final String receiverId = "receiver";
        final String nonParticipantId = "innocent";

        final BigDecimal senderBalanceBeforeTransfer = testApiClient.getBalance(senderId);
        final BigDecimal receiverBalanceBeforeTransfer = testApiClient.getBalance(receiverId);
        final BigDecimal nonParticipantBalanceBeforeTransfer = testApiClient.getBalance(nonParticipantId);
        final BigDecimal transferAmount = senderBalanceBeforeTransfer.subtract(BigDecimal.TEN);

        final TransferRequest transferRequest = TransferRequest.builder()
                .senderAccountId(senderId)
                .receiverAccountId(receiverId)
                .amount(transferAmount)
                .build();

        testApiClient.transfer(transferRequest);

        final BigDecimal senderBalanceAfterTransfer = testApiClient.getBalance(senderId);
        final BigDecimal receiverBalanceAfterTransfer = testApiClient.getBalance(receiverId);
        final BigDecimal nonParticipantBalanceAfterTransfer = testApiClient.getBalance(nonParticipantId);

        assertThat(senderBalanceAfterTransfer).isEqualTo(senderBalanceBeforeTransfer.subtract(transferAmount));
        assertThat(receiverBalanceAfterTransfer).isEqualTo(receiverBalanceBeforeTransfer.add(transferAmount));
        assertThat(nonParticipantBalanceBeforeTransfer).isEqualTo(nonParticipantBalanceAfterTransfer);
    }

    @Test
    @DisplayName("If funds are insufficient 400 is returned and balances stay the same")
    void test1() {
        final String senderId = "sender";
        final String receiverId = "receiver";
        final String nonParticipantId = "innocent";

        final BigDecimal senderBalanceBeforeTransfer = testApiClient.getBalance(senderId);
        final BigDecimal receiverBalanceBeforeTransfer = testApiClient.getBalance(receiverId);
        final BigDecimal nonParticipantBalanceBeforeTransfer = testApiClient.getBalance(nonParticipantId);
        final BigDecimal transferAmount = senderBalanceBeforeTransfer.add(BigDecimal.TEN);

        final TransferRequest transferRequest = TransferRequest.builder()
                .senderAccountId(senderId)
                .receiverAccountId(receiverId)
                .amount(transferAmount)
                .build();

        assertThatExceptionOfType(HttpClientResponseException.class)
                .isThrownBy(() -> testApiClient.transfer(transferRequest))
                .has(status(HttpStatus.BAD_REQUEST));

        final BigDecimal senderBalanceAfterTransfer = testApiClient.getBalance(senderId);
        final BigDecimal receiverBalanceAfterTransfer = testApiClient.getBalance(receiverId);
        final BigDecimal nonParticipantBalanceAfterTransfer = testApiClient.getBalance(nonParticipantId);

        assertThat(senderBalanceAfterTransfer).isEqualTo(senderBalanceBeforeTransfer);
        assertThat(receiverBalanceAfterTransfer).isEqualTo(receiverBalanceBeforeTransfer);
        assertThat(nonParticipantBalanceBeforeTransfer).isEqualTo(nonParticipantBalanceAfterTransfer);
    }

    @Test
    @DisplayName("If self-transfer, 400 is returned and balances stay the same")
    void test2() {
        final String senderId = "sender";
        final String receiverId = "receiver";
        final String nonParticipantId = "innocent";

        final BigDecimal senderBalanceBeforeTransfer = testApiClient.getBalance(senderId);
        final BigDecimal receiverBalanceBeforeTransfer = testApiClient.getBalance(receiverId);
        final BigDecimal nonParticipantBalanceBeforeTransfer = testApiClient.getBalance(nonParticipantId);
        final BigDecimal transferAmount = senderBalanceBeforeTransfer.subtract(BigDecimal.TEN);

        final TransferRequest transferRequest = TransferRequest.builder()
                .senderAccountId(senderId)
                .receiverAccountId(senderId)
                .amount(transferAmount)
                .build();

        assertThatExceptionOfType(HttpClientResponseException.class)
                .isThrownBy(() -> testApiClient.transfer(transferRequest))
                .has(status(HttpStatus.BAD_REQUEST));

        final BigDecimal senderBalanceAfterTransfer = testApiClient.getBalance(senderId);
        final BigDecimal receiverBalanceAfterTransfer = testApiClient.getBalance(receiverId);
        final BigDecimal nonParticipantBalanceAfterTransfer = testApiClient.getBalance(nonParticipantId);

        assertThat(senderBalanceAfterTransfer).isEqualTo(senderBalanceBeforeTransfer);
        assertThat(receiverBalanceAfterTransfer).isEqualTo(receiverBalanceBeforeTransfer);
        assertThat(nonParticipantBalanceBeforeTransfer).isEqualTo(nonParticipantBalanceAfterTransfer);
    }

    @Test
    @DisplayName("400 is the amount is negative")
    void test3() {
        final String senderId = "sender";
        final String receiverId = "receiver";
        final String nonParticipantId = "innocent";

        final BigDecimal senderBalanceBeforeTransfer = testApiClient.getBalance(senderId);
        final BigDecimal receiverBalanceBeforeTransfer = testApiClient.getBalance(receiverId);
        final BigDecimal nonParticipantBalanceBeforeTransfer = testApiClient.getBalance(nonParticipantId);
        final BigDecimal transferAmount = BigDecimal.TEN.negate();

        final TransferRequest transferRequest = TransferRequest.builder()
                .senderAccountId(senderId)
                .receiverAccountId(receiverId)
                .amount(transferAmount)
                .build();

        assertThatExceptionOfType(HttpClientResponseException.class)
                .isThrownBy(() -> testApiClient.transfer(transferRequest))
                .has(status(HttpStatus.BAD_REQUEST));

        final BigDecimal senderBalanceAfterTransfer = testApiClient.getBalance(senderId);
        final BigDecimal receiverBalanceAfterTransfer = testApiClient.getBalance(receiverId);
        final BigDecimal nonParticipantBalanceAfterTransfer = testApiClient.getBalance(nonParticipantId);

        assertThat(senderBalanceAfterTransfer).isEqualTo(senderBalanceBeforeTransfer);
        assertThat(receiverBalanceAfterTransfer).isEqualTo(receiverBalanceBeforeTransfer);
        assertThat(nonParticipantBalanceBeforeTransfer).isEqualTo(nonParticipantBalanceAfterTransfer);
    }

    private static Condition<HttpClientResponseException> status(final HttpStatus status) {
        return new Condition<>(
                e -> e.getStatus() == status,
                String.format("status %s", status)
        );
    }

}