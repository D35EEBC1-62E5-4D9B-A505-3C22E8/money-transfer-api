package mt.api.service;

import io.micronaut.test.annotation.MicronautTest;
import mt.api.error.AccountNotFoundException;
import mt.api.error.SelfTransferException;
import mt.api.model.TransferRequest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@MicronautTest
public final class TransferServiceTest {

    @Inject
    private TransferService service;

    @Test
    void transferThrowsSelfTransferExceptionIfFromAndToAccountIdsAreEqual() {
        final String accountId = "Doesn't exist anyway";
        final TransferRequest request = TransferRequest.builder()
                .senderAccountId(accountId)
                .receiverAccountId(accountId)
                .amount(BigDecimal.TEN)
                .build();

        assertThatExceptionOfType(SelfTransferException.class)
                .isThrownBy(() -> service.transfer(request));
    }

    @Test
    void transferThrowsAccountNotFoundExceptionIfFromAccountIsNotFound() {
        final TransferRequest request = TransferRequest.builder()
                .senderAccountId("Doesn't exist anyway")
                .receiverAccountId("payee")
                .amount(BigDecimal.TEN)
                .build();

        assertThatExceptionOfType(AccountNotFoundException.class)
                .isThrownBy(() -> service.transfer(request));
    }

}
