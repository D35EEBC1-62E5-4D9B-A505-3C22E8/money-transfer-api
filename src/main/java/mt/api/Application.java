package mt.api;

import io.micronaut.runtime.Micronaut;

public final class Application {

    public static void main(final String[] arguments) {
        Micronaut.run(Application.class, arguments);
    }

}