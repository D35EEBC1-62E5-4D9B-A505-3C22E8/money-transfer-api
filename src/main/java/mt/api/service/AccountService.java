package mt.api.service;

import mt.api.error.AccountNotFoundException;
import mt.api.model.Account;

import javax.inject.Singleton;
import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Singleton
public final class AccountService {

    private final Map<String, Account> accountById = new ConcurrentHashMap<>(Map.of(
            "sender", new Account("sender", new BigDecimal(1_000)),
            "receiver", new Account("receiver", new BigDecimal(300)),
            "innocent", new Account("innocent", new BigDecimal(1))
    ));

    public Account getById(final String id) {
        return Optional.ofNullable(accountById.get(id))
                .orElseThrow(() -> new AccountNotFoundException(id));
    }

    public Account save(final Account account) {
        accountById.put(account.getId(), account);
        return account;
    }

}
