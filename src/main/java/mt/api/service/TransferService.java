package mt.api.service;

import lombok.RequiredArgsConstructor;
import mt.api.error.SelfTransferException;
import mt.api.model.Account;
import mt.api.model.TransferRequest;

import javax.inject.Singleton;
import javax.validation.Valid;
import java.math.BigDecimal;

@Singleton
@RequiredArgsConstructor
public class TransferService {

    private final AccountService accounts;

    public TransferRequest transfer(@Valid final TransferRequest request) {
        final String senderAccountId = request.getSenderAccountId();
        final String receiverAccountId = request.getReceiverAccountId();

        if (senderAccountId.equals(receiverAccountId))
            throw new SelfTransferException(senderAccountId);

        // Both methods will throw in case it's not found
        final Account senderAccount = accounts.getById(senderAccountId);
        final Account receiverAccount = accounts.getById(receiverAccountId);

        final BigDecimal amount = request.getAmount();
        synchronized (senderAccount) {
            synchronized (receiverAccount) {
                senderAccount.withdraw(amount);
                receiverAccount.deposit(amount);
            }
        }

        return request;
    }

}
