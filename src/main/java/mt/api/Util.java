package mt.api;

import java.math.BigDecimal;

public final class Util {

    public static boolean isGreaterThan(final BigDecimal left, final BigDecimal right) {
        return left.compareTo(right) > 0;
    }

    public static boolean isPositive(final BigDecimal number) {
        return number.signum() > 0;
    }

    public static boolean isNonNegative(final BigDecimal number) {
        return number.signum() >= 0;
    }

    public static void requirePositive(final BigDecimal number) {
        if (number == null || !isPositive(number))
            throw new IllegalArgumentException(); // TODO: Add message
    }

    public static boolean areEqual(final BigDecimal left, final BigDecimal right) {
        return left.compareTo(right) == 0;
    }

    // Prevent instantiating
    private Util() {
    }

}
