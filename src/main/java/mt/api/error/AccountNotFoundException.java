package mt.api.error;

public final class AccountNotFoundException extends MoneyTransferException {

    private static String createMessage(final String accountId) {
        return String.format("Account with ID '%s' not found", accountId);
    }

    private final String accountId;

    public AccountNotFoundException(final String accountId) {
        super(createMessage(accountId));
        this.accountId = accountId;
    }

    public String getAccountId() {
        return accountId;
    }

}
