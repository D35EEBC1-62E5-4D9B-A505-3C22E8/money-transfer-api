package mt.api.error;

public final class NonSufficientFundsException extends MoneyTransferException {

    private static String createMessage(final String accountId) {
        return String.format("Account with ID '%s' has non-sufficient funds for transfer", accountId);
    }

    private final String accountId;

    public NonSufficientFundsException(final String accountId) {
        super(createMessage(accountId));
        this.accountId = accountId;
    }

    public String getAccountId() {
        return accountId;
    }

}
