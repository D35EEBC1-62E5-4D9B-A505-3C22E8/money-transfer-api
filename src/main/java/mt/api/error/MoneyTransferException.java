package mt.api.error;

public abstract class MoneyTransferException extends RuntimeException {

    public MoneyTransferException(final String message) {
        super(message);
    }

}
