package mt.api.error;

public final class SelfTransferException extends MoneyTransferException {

    private static String createMessage(final String accountId) {
        return String.format("Account with ID '%s' attempted self-transfer", accountId);
    }

    private final String accountId;

    public SelfTransferException(final String accountId) {
        super(createMessage(accountId));
        this.accountId = accountId;
    }

    public String getAccountId() {
        return accountId;
    }

}
