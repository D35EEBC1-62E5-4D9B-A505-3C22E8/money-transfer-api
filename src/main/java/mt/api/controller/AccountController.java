package mt.api.controller;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Error;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.hateoas.JsonError;
import io.micronaut.http.hateoas.Link;
import lombok.RequiredArgsConstructor;
import mt.api.model.Account;
import mt.api.error.AccountNotFoundException;
import mt.api.service.AccountService;

@Controller("/api/accounts")
@RequiredArgsConstructor
public final class AccountController {

    private final AccountService accounts;

    @Get("/{id}")
    public Account getById(final String id) {
        return accounts.getById(id);
    }

    @Error
    public HttpResponse<JsonError> onException(final HttpRequest request, final AccountNotFoundException exception) {
        final JsonError error = new JsonError(exception.getMessage())
                .link(Link.SELF, Link.of(request.getUri())); // TODO: Remove later. ATM for consistency

        return HttpResponse.notFound(error);
    }

}
