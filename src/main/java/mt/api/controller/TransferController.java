package mt.api.controller;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Error;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.hateoas.JsonError;
import io.micronaut.http.hateoas.Link;
import lombok.RequiredArgsConstructor;
import mt.api.error.MoneyTransferException;
import mt.api.model.TransferRequest;
import mt.api.service.TransferService;

@Controller("/api/transfers")
@RequiredArgsConstructor
public final class TransferController {

    private final TransferService transferService;

    @Post
    public TransferRequest returnIfValid(final TransferRequest request) {
        return transferService.transfer(request);
    }

    @Error
    public HttpResponse<JsonError> onException(final HttpRequest request, final MoneyTransferException exception) {
        final JsonError error = new JsonError(exception.getMessage())
                .link(Link.SELF, Link.of(request.getUri())); // TODO: Remove later. ATM for consistency

        return HttpResponse.badRequest(error);
    }

}
