package mt.api.model;

import io.micronaut.core.annotation.Introspected;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Data
@Builder // Mainly, for testing
@Introspected // Required for validation
public final class TransferRequest {

    /* Brief googling shows that payer and payee are more common in the domain
    however their usage here is purposely omitted as this terminology seems to
    be very error-prone because of auto-completion feature which is available
    in most modern IDEs */

    @NotNull
    private String senderAccountId;

    @NotNull
    private String receiverAccountId;

    @NotNull
    @Positive
    @Digits(integer = 3, fraction = 2)
    private BigDecimal amount;

}
