package mt.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import mt.api.Util;
import mt.api.error.NonSufficientFundsException;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public final class Account {

    private String id;
    private BigDecimal balance = BigDecimal.ZERO;

    public void withdraw(final BigDecimal amount) {
        Util.requirePositive(amount);

        final BigDecimal newBalance = balance.subtract(amount);
        if (Util.isNonNegative(newBalance))
            balance = newBalance;
        else
            throw new NonSufficientFundsException(id);
    }

    public void deposit(final BigDecimal amount) {
        Util.requirePositive(amount);
        balance = balance.add(amount);
    }

}
