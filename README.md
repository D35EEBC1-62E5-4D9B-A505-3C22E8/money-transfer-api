# Money Transfer API
## How to run?
```bash
mvn clean compile exec:exec
```
## Endpoints
* `GET /api/account/{id}`
* `POST /api/transfers`
